﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace fusion_sushi
{
    public class MenuItem
    {
        public BitmapImage image;
        public string name { get; }
        public bool ayce { get; }
        public double price { get; }
        public long filter;
        public MenuItemFragment uiMenuItemFragment;

        public MenuItem(string pictureSource, string name, bool ayce, double price, long filter)
        {
            image = new BitmapImage(new Uri(pictureSource, UriKind.Relative));
            this.name = name;
            this.ayce = ayce;
            this.price = price;
            this.filter = filter;
        }
    }
}
