﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace fusion_sushi
{
    /// <summary>
    /// Interaction logic for OrderItemFragment.xaml
    /// </summary>
    public partial class OrderItemFragment : UserControl
    {
        public OrderItem parent;

        public OrderItemFragment(OrderItem item, bool ayce)
        {
            InitializeComponent();
            Quantity.Content = item.qty.ToString();
            Description.Content = item.item.name;
            Price.Content = String.Format("{0:C2}", item.item.price); //"$" + item.item.price.ToString();

            this.parent = item;
            

            if (ayce && item.item.ayce)
            {
                AYCE.Content = "AYCE";
                
                Style strike = new Style(typeof(TextBlock));
                strike.Setters.Add(new Setter(TextBlock.TextDecorationsProperty, TextDecorations.Strikethrough));
                TextBlock tb = new TextBlock();
                tb.Text = "$" + item.item.price.ToString();
                tb.Style = strike;

                Price.Content = tb;

                BrushConverter bc = new BrushConverter();
                Price.Foreground = bc.ConvertFromString("#FFAAAAAA") as Brush;
            }

            if (item.status == STATUS.DONE)
            {
                Cancel.Visibility = Visibility.Hidden;
            }
            else
            {
                Status.Visibility = Visibility.Hidden;
            }
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            if (parent.item.name.Equals("AYCE Special"))
            {
                Application.Current.Windows.OfType<MainWindow>().FirstOrDefault().currentSelectedUser.ayce = false;
            }

            Application.Current.Windows.OfType<MainWindow>().FirstOrDefault().currentSelectedUser.orderedItems.Remove(parent);
            Application.Current.Windows.OfType<MainWindow>().FirstOrDefault().updateOrderPanel();
        }
        
    }
}
