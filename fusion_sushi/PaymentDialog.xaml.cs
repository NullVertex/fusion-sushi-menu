﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace fusion_sushi
{
    /// <summary>
    /// Interaction logic for PaymentDialog.xaml
    /// </summary>
    public partial class PaymentDialog : Window
    {
        public PaymentDialog(List<User> users)
        {
            InitializeComponent();

            double subtotal = 0.00;
            foreach (User user in users)
            {
                subtotal += user.subtotal();

                Grid grid = new Grid();
                grid.Width = 485;
                grid.Height = 85;

                ColumnDefinition c1 = new ColumnDefinition();
                c1.Width = new GridLength(85);
                ColumnDefinition c2 = new ColumnDefinition();
                c2.Width = new GridLength(1, GridUnitType.Star);
                ColumnDefinition c3 = new ColumnDefinition();
                c3.Width = new GridLength(85);
                grid.ColumnDefinitions.Add(c1);
                grid.ColumnDefinitions.Add(c2);
                grid.ColumnDefinitions.Add(c3);

                Image im = new Image();
                im.Source = user.userImage;
                im.Height = 65;
                im.Width = 65;
                Grid.SetColumn(im, 0);
                grid.Children.Add(im);

                Label nm = new Label();
                nm.Content = user.name;
                nm.VerticalContentAlignment = VerticalAlignment.Center;
                nm.HorizontalContentAlignment = HorizontalAlignment.Left;
                Grid.SetColumn(nm, 1);
                grid.Children.Add(nm);

                Label st = new Label();
                st.Content = String.Format("{0:C2}", user.subtotal());
                st.VerticalContentAlignment = VerticalAlignment.Center;
                st.HorizontalContentAlignment = HorizontalAlignment.Right;
                Grid.SetColumn(st, 2);
                grid.Children.Add(st);

                ContentPanel.Children.Insert(ContentPanel.Children.Count - 3, grid);
            }

            Subtotal.Content = String.Format("{0:C2}", subtotal);
            Tax.Content = String.Format("{0:C2}", subtotal * 0.05);
            Total.Content = String.Format("{0:C2}", subtotal * 1.05);
        }

        private void OkBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
