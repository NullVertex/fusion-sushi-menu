﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fusion_sushi
{
    public class STATUS
    {
        public const int UNSENT = 0;
        public const int SENT = 1;
        public const int DONE = 2;
    }

    public class OrderItem
    {
        public MenuItem item { get; set; }
        public int qty { get; set; }
        public int status { get; set; }

        public OrderItem(MenuItem i)
        {
            this.item = i;
            this.qty = 1;
            this.status = STATUS.UNSENT;
        }
        

    }
}
