﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace fusion_sushi
{
    static class Constants
    {
        public const long GLUTEN_FREE = 0x001;
        public const long DAIRY_FREE  = 0x010;
        public const long VEGAN_FREE  = 0x100;
        // public const short HOT        = 0x100; //TODO: Change hex value
        // public const short COLD       = 0x101; //TODO: Change hex value

        public const bool AYCE_YES = true;
        public const bool AYCE_NO = false;
    }

    class MenuDatabase
    {
        //private static List<MenuItem> menu = new List<MenuItem>();
        private static List<MenuCategory> menu = new List<MenuCategory>();
        private StackPanel menuPanel;
        private StackPanel catPanel;
        //private long currentFilter;

        public MenuDatabase(ref StackPanel panel, ref StackPanel categorypanel)
        {
            this.menuPanel = panel;
            this.catPanel = categorypanel;

            // Specials Category
            MenuCategory specials = new MenuCategory("Today's Specials");
            specials.Items.Add(new MenuItem("IMG/AvocadoRoll.png", "Avocado Roll (8 pcs)", Constants.AYCE_YES, 3.25, Constants.GLUTEN_FREE | Constants.DAIRY_FREE | Constants.VEGAN_FREE));
            specials.Items.Add(new MenuItem("IMG/BeefSkewers.png", "Beef Skewers (4 pcs)", Constants.AYCE_YES, 7.95, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            specials.Items.Add(new MenuItem("IMG/Calamari.png", "Calamari (8 pcs)", Constants.AYCE_YES, 6.95, Constants.DAIRY_FREE));
            specials.Items.Add(new MenuItem("IMG/CaliforniaRoll.png", "California Rolls (4 pcs)", Constants.AYCE_YES, 2.95, Constants.GLUTEN_FREE | Constants.DAIRY_FREE | Constants.VEGAN_FREE));
            specials.Items.Add(new MenuItem("IMG/OnionRings.png", "Onion Rings", Constants.AYCE_YES, 4.95, Constants.VEGAN_FREE));
            specials.Items.Add(new MenuItem("IMG/Takoyaki.png", "Takoyaki (6 pcs)", Constants.AYCE_YES, 5.95, Constants.GLUTEN_FREE));
            menu.Add(specials);

            MenuCategory drinks = new MenuCategory("Drinks");
            drinks.Items.Add(new MenuItem("IMG/BlackTea.png", "Black Tea", Constants.AYCE_NO, 2.00, Constants.GLUTEN_FREE | Constants.DAIRY_FREE | Constants.VEGAN_FREE));
            drinks.Items.Add(new MenuItem("IMG/GreenTea.png", "Green Tea", Constants.AYCE_NO, 2.00, Constants.GLUTEN_FREE | Constants.DAIRY_FREE | Constants.VEGAN_FREE));
            drinks.Items.Add(new MenuItem("IMG/Soda.png", "Pop (Can)", Constants.AYCE_NO, 2.00, Constants.GLUTEN_FREE | Constants.DAIRY_FREE | Constants.VEGAN_FREE));
            drinks.Items.Add(new MenuItem("IMG/WaterGlass.png", "Water", Constants.AYCE_NO, 0.00, Constants.GLUTEN_FREE | Constants.DAIRY_FREE | Constants.VEGAN_FREE));
            drinks.Items.Add(new MenuItem("IMG/WaterBottle.png", "Water (Bottle)", Constants.AYCE_NO, 2.00, Constants.GLUTEN_FREE | Constants.DAIRY_FREE | Constants.VEGAN_FREE));
            menu.Add(drinks);
            
            MenuCategory appetizers = new MenuCategory("Appetizers");
            appetizers.Items.Add(new MenuItem("IMG/Aged Tofu.png", "Aged Tofu (4 pcs)", Constants.AYCE_YES, 3.95, Constants.DAIRY_FREE));
            appetizers.Items.Add(new MenuItem("IMG/BeefSkewers.png", "Beef Skewers (4 pcs)", Constants.AYCE_YES, 7.95, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            appetizers.Items.Add(new MenuItem("IMG/Calamari.png", "Calamari (8 pcs)", Constants.AYCE_YES, 6.95, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            appetizers.Items.Add(new MenuItem("IMG/ChickenSkewers.png", "Chicken Skewers (4 pcs)", Constants.AYCE_YES, 5.95, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            appetizers.Items.Add(new MenuItem("IMG/Dumplings.png", "Dumplings (5 pcs)", Constants.AYCE_YES, 4.95, Constants.DAIRY_FREE));
            appetizers.Items.Add(new MenuItem("IMG/Edamame.jpg", "Edamame", Constants.AYCE_YES, 3.95, Constants.GLUTEN_FREE | Constants.DAIRY_FREE | Constants.VEGAN_FREE));
            appetizers.Items.Add(new MenuItem("IMG/Grilled Eggplant.jpg", "Grilled Eggplant (2 pcs)", Constants.AYCE_YES, 5.95, Constants.GLUTEN_FREE | Constants.DAIRY_FREE | Constants.VEGAN_FREE));
            appetizers.Items.Add(new MenuItem("IMG/Japanese Chicken Cutlet.jpg", "Japanese Chicken Cutlet", Constants.AYCE_YES, 6.95, Constants.DAIRY_FREE));
            appetizers.Items.Add(new MenuItem("IMG/Japanese Pork Cutlet.jpg", "Japanese Pork Cutlet", Constants.AYCE_YES, 6.95, Constants.DAIRY_FREE));
            appetizers.Items.Add(new MenuItem("IMG/OnionRings.png", "Onion Rings", Constants.AYCE_YES, 4.95, Constants.VEGAN_FREE));
            appetizers.Items.Add(new MenuItem("IMG/ShrimpSkewers.png", "Shrimp Skewers (3 pcs)", Constants.AYCE_YES, 7.95, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            appetizers.Items.Add(new MenuItem("IMG/Takoyaki.png", "Takoyaki (6 pcs)", Constants.AYCE_YES, 5.95, Constants.GLUTEN_FREE));
            menu.Add(appetizers);

            MenuCategory rolls = new MenuCategory("Rolls");
            rolls.Items.Add(new MenuItem("IMG/AvocadoRoll.png", "Avocado Roll (8 pcs)", Constants.AYCE_YES, 3.25, Constants.GLUTEN_FREE | Constants.DAIRY_FREE | Constants.VEGAN_FREE));
            rolls.Items.Add(new MenuItem("IMG/CaliforniaRoll.png", "California Roll (4 pcs)", Constants.AYCE_YES, 2.95, Constants.GLUTEN_FREE | Constants.DAIRY_FREE | Constants.VEGAN_FREE));
            rolls.Items.Add(new MenuItem("IMG/Philadelphia Roll.jpg", "Philadelphia Roll (8 pcs)", Constants.AYCE_YES, 4.95, Constants.GLUTEN_FREE));
            rolls.Items.Add(new MenuItem("IMG/CucumberRoll.png", "Cucumber Roll (8 pcs)", Constants.AYCE_YES, 3.25, Constants.GLUTEN_FREE | Constants.DAIRY_FREE | Constants.VEGAN_FREE));
            rolls.Items.Add(new MenuItem("IMG/Crab Roll.jpg", "Crab Roll (8 pcs)", Constants.AYCE_YES, 3.50, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            rolls.Items.Add(new MenuItem("IMG/Salmon Sushi Roll.jpg", "Salmon Roll (8 pcs)", Constants.AYCE_YES, 3.95, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            rolls.Items.Add(new MenuItem("IMG/Tuna Roll.jpeg", "Tuna Roll (8 pcs)", Constants.AYCE_YES, 3.95, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            rolls.Items.Add(new MenuItem("IMG/Spicy Crab Roll.jpg", "Spicy Crab Roll (8 pcs)", Constants.AYCE_YES, 3.50, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            rolls.Items.Add(new MenuItem("IMG/Spicy Salmon Roll.jpg", "Spicy Salmon Roll (8 pcs)", Constants.AYCE_YES, 3.95, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            rolls.Items.Add(new MenuItem("IMG/Spicy Tuna Roll.jpg", "Spicy Tuna Roll (8 pcs)", Constants.AYCE_YES, 3.95, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            menu.Add(rolls);

            MenuCategory specialty = new MenuCategory("Specialty Rolls");
            specialty.Items.Add(new MenuItem("IMG/Fruit Roll.jpg", "Fruit Roll", Constants.AYCE_YES, 8.99, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            specialty.Items.Add(new MenuItem("IMG/Futo Roll.jpg", "Futo Roll (5 pcs)", Constants.AYCE_NO, 10.95, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            specialty.Items.Add(new MenuItem("IMG/Sakura Roll.jpg", "Sakura Roll (8 pcs)", Constants.AYCE_YES, 8.99, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            specialty.Items.Add(new MenuItem("IMG/Volcano Roll.jpg", "Volcano Roll", Constants.AYCE_YES, 13.95, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            menu.Add(specialty);

            MenuCategory sashimi = new MenuCategory("Sashimi");
            sashimi.Items.Add(new MenuItem("IMG/Egg Sashimi.jpg", "Egg Sashimi (3 pcs)", Constants.AYCE_YES, 4.00, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            sashimi.Items.Add(new MenuItem("IMG/Mackerel Sashimi.jpg", "Mackerel Sashimi (3 pcs)", Constants.AYCE_YES, 5.50, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            sashimi.Items.Add(new MenuItem("IMG/Octopus Sashimi.jpg", "Octopus Sashimi (3 pcs)", Constants.AYCE_YES, 5.95, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            sashimi.Items.Add(new MenuItem("IMG/Salmon Sashimi.jpg", "Salmon Sashimi (3 pcs)", Constants.AYCE_YES, 5.50, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            sashimi.Items.Add(new MenuItem("IMG/Shrimp Sashimi.jpg", "Shrimp Sashimi (3 pcs)", Constants.AYCE_YES, 5.00, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            sashimi.Items.Add(new MenuItem("IMG/Tuna Sashimi.jpg", "Tuna Sashimi (3 pcs)", Constants.AYCE_YES, 5.95, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            menu.Add(sashimi);

            MenuCategory nigiri = new MenuCategory("Nigiri");
            nigiri.Items.Add(new MenuItem("IMG/Crab Meat Nigiri.jpg", "Crab Meat Nigiri (2 pcs)", Constants.AYCE_YES, 3.00, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            nigiri.Items.Add(new MenuItem("IMG/Egg Nigiri.jpeg", "Egg Nigiri (2 pcs)", Constants.AYCE_YES, 3.00, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            nigiri.Items.Add(new MenuItem("IMG/Octopus Nigiri.jpg", "Octopus Nigiri (2 pcs)", Constants.AYCE_YES, 4.00, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            nigiri.Items.Add(new MenuItem("IMG/Salmon Nigiri.jpg", "Salmon Nigiri (2 pcs)", Constants.AYCE_YES, 4.50, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            nigiri.Items.Add(new MenuItem("IMG/Tofu Nigiri.jpg", "Tofu Skin Nigiri (2 pcs)", Constants.AYCE_YES, 3.00, Constants.GLUTEN_FREE | Constants.DAIRY_FREE | Constants.VEGAN_FREE));
            nigiri.Items.Add(new MenuItem("IMG/Tuna Nigiri.jpg", "Tuna Nigiri (2 pcs)", Constants.AYCE_YES, 4.00, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            menu.Add(nigiri);

            MenuCategory tempura = new MenuCategory("Tempura");
            tempura.Items.Add(new MenuItem("IMG/CrabTempura.png", "Crab Tempura (6pcs)", Constants.AYCE_YES, 6.95, Constants.DAIRY_FREE));
            tempura.Items.Add(new MenuItem("IMG/ShrimpTempura.png", "Shrimp Tempura (5 pcs)", Constants.AYCE_YES, 7.95, Constants.DAIRY_FREE));
            tempura.Items.Add(new MenuItem("IMG/VegTempura.png", "Veggie Tempura (8 pcs)", Constants.AYCE_YES, 5.95, Constants.DAIRY_FREE | Constants.VEGAN_FREE));
            tempura.Items.Add(new MenuItem("IMG/YamTempura.png", "Yam Tempura (8 pcs)", Constants.AYCE_YES, 5.95, Constants.DAIRY_FREE | Constants.VEGAN_FREE));
            menu.Add(tempura);

            MenuCategory teriyaki = new MenuCategory("Teriyaki");
            teriyaki.Items.Add(new MenuItem("IMG/Beef Teriyaki.jpg", "Beef Teriyaki (8 pcs)", Constants.AYCE_YES, 3.95, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            teriyaki.Items.Add(new MenuItem("IMG/Chicken Teriyaki.jpg", "Chicken Teriyaki (8 pcs)", Constants.AYCE_YES, 3.95, Constants.GLUTEN_FREE | Constants.DAIRY_FREE));
            menu.Add(teriyaki);

            RefreshMenu(menu);
        }

        public void RefreshMenu(List<MenuCategory> menu)
        {
            // Remove everything except AYCE button
            if (menuPanel.Children.Count > 1)
            {
                menuPanel.Children.RemoveRange(1, menuPanel.Children.Count - 1);
            }
            // Clear categories
            if (catPanel.Children.Count > 6)
            {
                catPanel.Children.RemoveRange(6, catPanel.Children.Count - 6);
            }

            foreach (MenuCategory category in menu)
            {
                if (category.Items.Count > 0)
                {
                    WrapPanel cat_contents = new WrapPanel();
                    foreach (MenuItem item in category.Items)
                    {
                        cat_contents.Children.Add(new MenuItemFragment(item));
                    }

                    BrushConverter bc = new BrushConverter();

                    Label header = new Label();
                    header.Content = category.CategoryName;
                    header.FontWeight = FontWeights.Bold;
                    header.Padding = new Thickness(10, 40, 10, 0);
                    header.Foreground = (Brush)bc.ConvertFromString("#ff5a168d");
                    header.FontSize = 24;
                    menuPanel.Children.Add(header);

                    Button cat = new Button();
                    cat.Content = category.CategoryName;
                    cat.FontSize = 20;
                    cat.Height = 40;
                    
                    cat.Background = (Brush) bc.ConvertFrom(Colors.Transparent.ToString());
                    cat.BorderThickness = new Thickness(0,0,0,0);
                    catPanel.Children.Add(cat);
                    cat.Click += (source, e) =>
                    {
                        header.BringIntoView();
                        cat_contents.BringIntoView();
                    };

                    menuPanel.Children.Add(cat_contents);
                }
            }
        }
        
        public void FilterMenu(long filter)
        {
            // Apply filters
            List<MenuCategory> modified_menu = new List<MenuCategory>();

            foreach (MenuCategory category in menu)
            {
                MenuCategory modified_cat = new MenuCategory(category.CategoryName);
                modified_cat.Items = category.FilterItems(filter);
                modified_menu.Add(modified_cat);
            }

            RefreshMenu(modified_menu);
        }

        public void searchMenu(string search, long filter)
        {
            if (search.Equals(""))
            {
                RefreshMenu(menu);
            }
            else
            {
                // Apply filters
                List<MenuCategory> modified_menu = new List<MenuCategory>();

                foreach (MenuCategory category in menu)
                {
                    MenuCategory modified_cat = new MenuCategory(category.CategoryName);
                    modified_cat.Items = category.FilterItems(filter);
                    MenuCategory searched_cat = new MenuCategory(category.CategoryName);
                    foreach (MenuItem item in modified_cat.Items)
                    {
                        if (item.name.ToLower().Contains(search.ToLower()))
                        {
                            searched_cat.Items.Add(item);
                        }
                    }
                    modified_menu.Add(searched_cat);
                }

                RefreshMenu(modified_menu);
            }
        }
    }
}
