﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace fusion_sushi
{
    public class User
    {
        public int userId;
        public string name;
        public BitmapImage userImage { get; set; }
        public List<OrderItem> orderedItems;
        public UserFragment uiUser;
        public bool ayce;
        public long filter { get; set; }

        public User(ref StackPanel orderPanel, int userId)
        {
            this.userId = userId;
            this.name = "User " + userId;
            this.userImage = new BitmapImage(new Uri(getStringOfImage(), UriKind.Relative));
            this.orderedItems = new List<OrderItem>();
            this.ayce = false;
            this.filter = 0x000;
        }

        //Assigns an image and name for each new user
        private string getStringOfImage()
        {
            if (userId == 1)
            {
                name = "Rat";
                return "IMG/User_Images/Rat.png";
            }
            else if (userId == 2)
            {
                name = "Ox";
                return "IMG/User_Images/OX.png";
            }
            else if (userId == 3)
            {
                name = "Tiger";
                return "IMG/User_Images/Tiger.png";
            }
            else if (userId == 4)
            {
                name = "Rabbit";
                return "IMG/User_Images/Rabbit.png";
            }
            else if (userId == 5)
            {
                name = "Dragon";
                return "IMG/User_Images/Dragon.png";
            }
            else if (userId == 6)
            {
                name = "Snake";
                return "IMG/User_Images/Snake.png";
            }
            else if (userId == 7)
            {
                name = "Horse";
                return "IMG/User_Images/Horse.png";
            }
            else if (userId == 8)
            {
                name = "Goat";
                return "IMG/User_Images/Goat.png";
            }
            else if (userId == 9)
            {
                name = "Monkey";
                return "IMG/User_Images/Monkey.png";
            }
            else if (userId == 10)
            {
                name = "Rooster";
                return "IMG/User_Images/Rooster.png";
            }
            else if (userId == 11)
            {
                name = "Dog";
                return "IMG/User_Images/Dog.png";
            }
            else if (userId == 12)
            {
                name = "Pig";
                return "IMG/User_Images/Pig.png";
            }

            return "IMG/DEFAULT_USER.png";
        }

        public void AddToOrder(MenuItem item)
        {
            bool foundmatch = false;
            foreach (OrderItem i in orderedItems)
            {
                if (i.item.name.Equals(item.name) && i.status == STATUS.UNSENT)
                {
                    i.qty++;
                    foundmatch = true;
                }
            }

            if (!foundmatch)
            {
                orderedItems.Add(new OrderItem(item));
            }
        }
        
        public double subtotal()
        {
            double st = 0.00;
            if(ayce)
            {
                st = 27.50;

                foreach (OrderItem i in orderedItems)
                {
                    if(i.item.ayce == Constants.AYCE_NO && i.status == STATUS.DONE)
                    {
                        st += (i.qty * i.item.price);
                    }
                }
            }
            else
            {
                foreach (OrderItem i in orderedItems)
                {
                    if (i.status == STATUS.DONE)
                    {
                        st += (i.qty * i.item.price);
                    }
                }
            }

            return st;
        }
        
    }
}
