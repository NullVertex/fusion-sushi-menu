﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace fusion_sushi
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class MenuItemFragment : UserControl
    {
        public MenuItem item;

        public MenuItemFragment(MenuItem item)
        {
            InitializeComponent();
            this.item = item;
            ItemImage.Source = item.image;
            ItemName.Content = item.name;
            ItemCost.Content = String.Format("{0:C2}", item.price);

            if ((item.filter & Constants.GLUTEN_FREE) == Constants.GLUTEN_FREE) 
            {
                GlutenFreeImage.Visibility = Visibility.Visible;
            }

            if ((item.filter & Constants.VEGAN_FREE) == Constants.VEGAN_FREE)
            {
                VeganFreeImage.Visibility = Visibility.Visible;
            }

            if ((item.filter & Constants.DAIRY_FREE) == Constants.DAIRY_FREE)
            {
                DairyFreeImage.Visibility = Visibility.Visible;
            }
            if(item.ayce)
            {
                ayce_tag.Visibility = Visibility.Visible;
            }
        }

        private void MenuItemButton_Click(object sender, RoutedEventArgs e)
        {
            //Application.Current.Windows.OfType<MainWindow>().FirstOrDefault().currentSelectedUser.orderedItems.Add(item);
            Application.Current.Windows.OfType<MainWindow>().FirstOrDefault().currentSelectedUser.AddToOrder(item);
            Application.Current.Windows.OfType<MainWindow>().FirstOrDefault().updateOrderPanel();
        }
    }
}
