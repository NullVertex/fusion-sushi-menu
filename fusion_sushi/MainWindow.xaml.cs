﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace fusion_sushi
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MenuDatabase database;
        public List<User> users;
        public User currentSelectedUser;
        public WrapPanel searchPanel;
        public Popup userImageSelector;

        public MainWindow()
        {
            InitializeComponent();
            users = new List<User>();

            //Set up User 1
            User initUser = new User(ref OrderedItems, 1);
            UserFragment userFragmentToAdd = new UserFragment(initUser);
            userFragmentToAdd.UserButton.Background = (Brush)(new BrushConverter().ConvertFrom("#FFDDDDDD"));
            initUser.uiUser = userFragmentToAdd;
            users.Add(initUser);
            UserPanel.Children.Insert(0, userFragmentToAdd);
            currentSelectedUser = users[0];

            //Will Initialize MenuItems into Wrap Panel 
            database = new MenuDatabase(ref MenuContainer, ref CategoryPanel);
            setupUserImageSelector();
        }

        private void OrderTabHeaderClick(object sender, RoutedEventArgs e)
        {
            FilterPanel.Visibility = Visibility.Hidden;
            OrderPanel.Visibility = Visibility.Visible;
            OrderTabHeader.IsEnabled = false;
            FilterTabHeader.IsEnabled = true;

            updateOrderPanel();
        }

        private void FilterTabHeaderClick(object sender, RoutedEventArgs e)
        {
            OrderPanel.Visibility = Visibility.Hidden;
            FilterPanel.Visibility = Visibility.Visible;
            OrderTabHeader.IsEnabled = true;
            FilterTabHeader.IsEnabled = false;
        }

        private void FilterCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            long filter = 0x000;

            if (checkGluten.IsChecked == true)
            {
                filter ^= Constants.GLUTEN_FREE;
            }
            if (checkDairy.IsChecked == true)
            {
                filter ^= Constants.DAIRY_FREE;
            }
            if (checkVegan.IsChecked == true)
            {
                filter ^= Constants.VEGAN_FREE;
            }

            if(textBox.Text.Equals("Search..."))
                database.FilterMenu(filter);
            else
            {
                //database.filterSearchResults(filter);
                database.searchMenu(textBox.Text, filter);
            }
        }

        private void Add_User_Button_Click(object sender, RoutedEventArgs e)
        {
            //By default when adding a user, they will be doing the AYCE Special (for now)
            User userToAdd = new User(ref OrderedItems, users.Count + 1);
            //userToAdd.orderedItems.Add(new OrderItem(new MenuItem("", "AYCE Special", Constants.AYCE_NO, 27.50, 0x0)));
            UserFragment userFragmentToAdd = new UserFragment(userToAdd);
            userToAdd.uiUser = userFragmentToAdd;
            users.Add(userToAdd);
            //UserPanel.Children.Add(userFragmentToAdd);
            UserPanel.Children.Insert(UserPanel.Children.Count - 1, userFragmentToAdd);

            if (users.Count >= 12)
            {
                AddUserButton.Visibility = Visibility.Collapsed;
            }
            userFragmentToAdd.UserButton_Click(sender, e);
        }

        public void updateOrderPanel()
        {
            username.Content = currentSelectedUser.name;
            OrderPanel.Children.OfType<TextBox>().First(i => i.Name == "usernameEditor").Text = currentSelectedUser.name;
            Image userImage = (Image)OrderPanel.Children.OfType<Button>().First(i => i.Name == "userimageButton").Template.FindName("userimage", OrderPanel.Children.OfType<Button>().First(i => i.Name == "userimageButton"));
            userImage.Source = currentSelectedUser.userImage;

            //OrderedItems.Children.Clear();
            if (OrderedItems.Children.Count > 1)
            {
                OrderedItems.Children.RemoveRange(1, OrderedItems.Children.Count - 1);
            }

            if(currentSelectedUser.ayce)
            {
                OrderedItems.Children.Add(new OrderItemFragment(new OrderItem(new MenuItem("", "AYCE Special", Constants.AYCE_NO, 27.50, 0x0)), Constants.AYCE_NO));
            }
            for (int i = 0; i < currentSelectedUser.orderedItems.Count; i++)
            {
                OrderedItems.Children.Add(new OrderItemFragment(currentSelectedUser.orderedItems.ElementAt(i), currentSelectedUser.ayce));
            }

            if(currentSelectedUser.ayce)
            {
                AYCE_Button.Visibility = Visibility.Collapsed;
            }
            else
            {
                AYCE_Button.Visibility = Visibility.Visible;
            }

            int unsentItems = 0;
            foreach (OrderItem i in currentSelectedUser.orderedItems)
            {
                if (i.status == STATUS.UNSENT)
                {
                    unsentItems += i.qty;
                }
            }

            if (unsentItems > 0)
            {
                OrderTabHeader.Content = String.Format("({0}) My Order", unsentItems);
                SubmitButton.IsEnabled = true;
            }
            else
            {
                OrderTabHeader.Content = "My Order";
                SubmitButton.IsEnabled = false;
            }
        }

        private void SubmitUnsentItems(object sender, RoutedEventArgs e)
        {
            foreach (OrderItem i in currentSelectedUser.orderedItems)
            {
                if (i.status == STATUS.UNSENT)
                {
                    i.status = STATUS.DONE;
                    updateOrderPanel();
                }
            }
        }

        private void PayButtonClick(object sender, RoutedEventArgs e)
        {
            PaymentDialog payment = new PaymentDialog(users);
            payment.Show();

            //currentSelectedUser.name = "$" + subtotal;
        }

        private void AYCE_Click(object sender, RoutedEventArgs e)
        {
            currentSelectedUser.ayce = true;
            updateOrderPanel();
            AYCE_Button.Visibility = Visibility.Collapsed;
        }

        private void Search_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (textBox.Text.Equals("Search..."))
            {
                textBox.Text = "";
            }
        }

        private void Search_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (textBox.Text.Equals(""))
            {
                textBox.Text = "Search...";
            }
        }

        //Everytime the text changes, this function is called
        private void Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            long filter = 0x000;

            if (checkGluten != null && checkGluten.IsChecked == true)
            {
                filter ^= Constants.GLUTEN_FREE;
            }
            if (checkDairy != null && checkDairy.IsChecked == true)
            {
                filter ^= Constants.DAIRY_FREE;
            }
            if (checkVegan != null && checkVegan.IsChecked == true)
            {
                filter ^= Constants.VEGAN_FREE;
            }

            if (database != null)
            {
                if (textBox.Text.Equals("Search...") || textBox.Text.Equals(""))
                {
                    database.FilterMenu(filter);
                }
                else
                {
                    database.searchMenu(textBox.Text, filter);
                }
            }
        }

        //save username
        private void usernameEditor_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (!usernameEditor.Text.Equals(""))
            {
                currentSelectedUser.name = usernameEditor.Text;
            }
            usernameEditor.Visibility = Visibility.Collapsed;
            updateOrderPanel();
        }

        private void nameGainFocus(object sender, RoutedEventArgs e)
        {
            usernameEditor.Text = "";
            usernameEditor.Visibility = Visibility.Visible;
            Keyboard.Focus(usernameEditor);
        }

        //Show popup of user images to choose from
        private void userimageButton_Click(object sender, RoutedEventArgs e)
        {
            userImageSelector.PlacementTarget = sender as Button;
            userImageSelector.IsOpen = true;
        }

        //Update current selected user images
        private void imageSelected(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            
            currentSelectedUser.userImage = new BitmapImage(new Uri("IMG/User_Images/" + b.Name + ".png", UriKind.Relative));

            //Set OrderPanel User Image
            Image userImage = (Image)OrderPanel.Children.OfType<Button>().First(i => i.Name == "userimageButton").Template.FindName("userimage", OrderPanel.Children.OfType<Button>().First(i => i.Name == "userimageButton"));
            userImage.Source = currentSelectedUser.userImage;
            
            //Set User Stack Panel User Image
            foreach (UserFragment u in UserPanel.Children.OfType<UserFragment>())
            {
                if (u.user.userId == currentSelectedUser.userId)
                {
                    u.user.userImage = currentSelectedUser.userImage;
                    u.UserImage.Source = currentSelectedUser.userImage;
                    break;
                }
            }
        }

        private void setupUserImageSelector()
        {
            userImageSelector = new Popup();
            StackPanel imageButtons = new StackPanel();
            imageButtons.Orientation = Orientation.Vertical;

            StackPanel row1 = new StackPanel();
            row1.Orientation = Orientation.Horizontal;
            StackPanel row2 = new StackPanel();
            row2.Orientation = Orientation.Horizontal;
            StackPanel row3 = new StackPanel();
            row3.Orientation = Orientation.Horizontal;

            Button ratButton = new Button
            {
                Name = "Rat",
                Width = 60,
                Height = 60,
                Content = new Image
                {
                    Source = new BitmapImage(new Uri("IMG/User_Images/Rat.png", UriKind.Relative)),
                    VerticalAlignment = VerticalAlignment.Center
                }
            };
            ratButton.Click += new RoutedEventHandler(imageSelected);
            row1.Children.Add(ratButton);

            Button oxButton = new Button
            {
                Name = "OX",
                Width = 60,
                Height = 60,
                Content = new Image
                {
                    Source = new BitmapImage(new Uri("IMG/User_Images/OX.png", UriKind.Relative)),
                    VerticalAlignment = VerticalAlignment.Center
                }
            };
            oxButton.Click += new RoutedEventHandler(imageSelected);
            row1.Children.Add(oxButton);

            Button tigerButton = new Button
            {
                Name = "Tiger",
                Width = 60,
                Height = 60,
                Content = new Image
                {
                    Source = new BitmapImage(new Uri("IMG/User_Images/Tiger.png", UriKind.Relative)),
                    VerticalAlignment = VerticalAlignment.Center
                }
            };
            tigerButton.Click += new RoutedEventHandler(imageSelected);
            row1.Children.Add(tigerButton);

            Button rabbitButton = new Button
            {
                Name = "Rabbit",
                Width = 60,
                Height = 60,
                Content = new Image
                {
                    Source = new BitmapImage(new Uri("IMG/User_Images/Rabbit.png", UriKind.Relative)),
                    VerticalAlignment = VerticalAlignment.Center
                }
            };
            rabbitButton.Click += new RoutedEventHandler(imageSelected);
            row1.Children.Add(rabbitButton);
            
            Button dragonButton = new Button
            {
                Name = "Dragon",
                Width = 60,
                Height = 60,
                Content = new Image
                {
                    Source = new BitmapImage(new Uri("IMG/User_Images/Dragon.png", UriKind.Relative)),
                    VerticalAlignment = VerticalAlignment.Center
                }
            };
            dragonButton.Click += new RoutedEventHandler(imageSelected);
            row2.Children.Add(dragonButton);

            Button snakeButton = new Button
            {
                Name = "Snake",
                Width = 60,
                Height = 60,
                Content = new Image
                {
                    Source = new BitmapImage(new Uri("IMG/User_Images/Snake.png", UriKind.Relative)),
                    VerticalAlignment = VerticalAlignment.Center
                }
            };
            snakeButton.Click += new RoutedEventHandler(imageSelected);
            row2.Children.Add(snakeButton);

            Button horseButton = new Button
            {
                Name = "Horse",
                Width = 60,
                Height = 60,
                Content = new Image
                {
                    Source = new BitmapImage(new Uri("IMG/User_Images/Horse.png", UriKind.Relative)),
                    VerticalAlignment = VerticalAlignment.Center
                }
            };
            horseButton.Click += new RoutedEventHandler(imageSelected);
            row2.Children.Add(horseButton);

            Button goatButton = new Button
            {
                Name = "Goat",
                Width = 60,
                Height = 60,
                Content = new Image
                {
                    Source = new BitmapImage(new Uri("IMG/User_Images/Goat.png", UriKind.Relative)),
                    VerticalAlignment = VerticalAlignment.Center
                }
            };
            goatButton.Click += new RoutedEventHandler(imageSelected);
            row2.Children.Add(goatButton);

            Button monkeyButton = new Button
            {
                Name = "Monkey",
                Width = 60,
                Height = 60,
                Content = new Image
                {
                    Source = new BitmapImage(new Uri("IMG/User_Images/Monkey.png", UriKind.Relative)),
                    VerticalAlignment = VerticalAlignment.Center
                }
            };
            monkeyButton.Click += new RoutedEventHandler(imageSelected);
            row3.Children.Add(monkeyButton);

            Button roosterButton = new Button
            {
                Name = "Rooster",
                Width = 60,
                Height = 60,
                Content = new Image
                {
                    Source = new BitmapImage(new Uri("IMG/User_Images/Rooster.png", UriKind.Relative)),
                    VerticalAlignment = VerticalAlignment.Center
                }
            };
            roosterButton.Click += new RoutedEventHandler(imageSelected);
            row3.Children.Add(roosterButton);

            Button dogButton = new Button
            {
                Name = "Dog",
                Width = 60,
                Height = 60,
                Content = new Image
                {
                    Source = new BitmapImage(new Uri("IMG/User_Images/Dog.png", UriKind.Relative)),
                    VerticalAlignment = VerticalAlignment.Center
                }
            };
            dogButton.Click += new RoutedEventHandler(imageSelected);
            row3.Children.Add(dogButton);

            Button pigButton = new Button
            {
                Name = "Pig",
                Width = 60,
                Height = 60,
                Content = new Image
                {
                    Source = new BitmapImage(new Uri("IMG/User_Images/Pig.png", UriKind.Relative)),
                    VerticalAlignment = VerticalAlignment.Center
                }
            };
            pigButton.Click += new RoutedEventHandler(imageSelected);
            row3.Children.Add(pigButton);

            imageButtons.Children.Add(row1);
            imageButtons.Children.Add(row2);
            imageButtons.Children.Add(row3);

            userImageSelector.Child = imageButtons;
            userImageSelector.StaysOpen = false;
        }

        private void Server_Click(object sender, RoutedEventArgs e)
        {
            ServerDialog sd = new ServerDialog();
            sd.Show();
        }
    }
}
