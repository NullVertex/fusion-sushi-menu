﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace fusion_sushi
{
    /// <summary>
    /// Interaction logic for UserFragment.xaml
    /// </summary>
    public partial class UserFragment : UserControl
    {
        public User user;

        public UserFragment(User user)
        {
            InitializeComponent();
            this.user = user;
            UserImage.Source = this.user.userImage;
            
        }

        public void UserButton_Click(object sender, RoutedEventArgs e)
        {
            if(Application.Current.Windows.OfType<MainWindow>().FirstOrDefault().users.Count > 1)
                Application.Current.Windows.OfType<MainWindow>().FirstOrDefault().currentSelectedUser.uiUser.UserButton.Background = Brushes.White;

            Application.Current.Windows.OfType<MainWindow>().FirstOrDefault().currentSelectedUser = this.user;
            UserButton.Background = (Brush)(new BrushConverter().ConvertFrom("#FFDDDDDD"));

            Application.Current.Windows.OfType<MainWindow>().FirstOrDefault().updateOrderPanel();
        }
    }
}
