﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fusion_sushi
{
    class MenuCategory
    {
        public String CategoryName { get; }
        public List<MenuItem> Items { get; set; }

        public MenuCategory(String name)
        {
            this.CategoryName = name;
            this.Items = new List<MenuItem>();
        }

        public List<MenuItem> FilterItems(long filter)
        {
            List<MenuItem> results = new List<MenuItem>();
            //long currentFilter = 0x0;

            foreach (MenuItem item in Items)
            {
                if ((item.filter & filter) == filter)
                {
                    results.Add(item);
                }
            }

            return results;
        }
    }
}
