# Fusion Sushi (CPSC 481 T02, Group 5 )#

### Student Information ###
 - [Anthony Chen, 10097921](mailto://anthony.chen@ucalgary.ca)  
 - [Mitchell Rudy, 10160634](mailto://mitchell.rudy@ucalgary.ca)  
 - [Cole Towstego, 10089693](mailto://ctowsteg@ucalgary.ca)  

### How to run ###
To run this application, either:

 - execute the file `<APP_PATH>/fusion_sushi/bin/Release/fusion_sushi.exe`
 - Open `<APP_PATH>/fusion_sushi/fusion_sushi.csproj` in visual studio and press the `Start` button